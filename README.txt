DLX Simulator
~~~~~~~~~~~~~

by David Viner (post@davidviner.com)

Legal Stuff
~~~~~~~~~~~

This program is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation; either version 2 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program; if not, write to the Free Software Foundation, Inc., 59 Temple
Place, Suite 330, Boston, MA  02111-1307  USA

Anyone wishing to include this software with any other bundle is totally free
to do so provided they adhere to the rules of the GNU GPL.

Introduction
~~~~~~~~~~~~

Between 1993 and 1996 I took an Applied Computing degree course at the
University of East Anglia in Norwich, Norfolk, England. In the final year I had
to produce a project. I chose to simulate the DLX CPU as described in the first
edition of the book "Computer Architecture: A Quantitative Approach", by John L
Hennessy and David A Patterson (subsequently referred to as H&P) as this had
been used extensively in part of the second year of the course. I have now
released the project as free (GNU GPL) software so that anyone else can make
use of or expand upon the system as required.

Note that I do not claim that this simulator is any better than any others that
are now out there. However, the source code is available and you can tinker
with it in any way you want (if you make vast improvements then please email me
a copy and the reasons for the changes).

The software is supplied as source code only but with a Makefile that will
allow it to be compiled using the GCC compiler. The source is written in ANSI
standard C and has previously been compiled under Unix, Linux, PC (Turbo C++)
and Amiga (SAS C).

The H&P book has been released in a second edition and many of the references
in the manual and the course code will probably no longer apply. Ah well,
that's progress :-)

Source Code
~~~~~~~~~~~

Each program is made up of several source code modules whose name and usage is
as follows:

cpu.c       Routines common to all simulator modules: instruction decode
            tables; decode register routines; cpu initialisation; cpu register
            clearing; timer handling; entry point for shell calls Run and
            RunOneInstruction.

cpuh.c      Hardwired simulator.

cpum.c      Microcode simulator.

cpup.c      Pipelined simulator.

dasm.c      Main DAsm source.

dasm2.c     Opcode table, error handling and commonly used subroutines for DAsm.

dasmlink.c  Contains the date of compilation and link number for DAsm.

dsym.c      Label and symbol table binary tree handling for DAsm.

dis.c       Disassembler for Mon.

help.c      On-line help for Mon.

ini.c       Initialisation file source for Mon.

io.c        File i/o for program load and save in Mon.

masm.c      Source for MAsm.

masmlink.c  Contains the date of compilation and link number for MAsm (this was
            only ever used on the Amiga which was my main platform while the
            project was being created).

mem.c       DLX simulator memory input and output routines for Mon.

mon.c       Main loop and command routines for Mon.

mon2.c      Commonly used routines for Mon: user input and output; version
            display; short help display.

mon3.c      Simulator interface for Mon. Provides debugging and screen input
            and output.

monlink.c   Contains the date of compilation and link number for Mon (Amiga
            only).

msym.c      Label and symbol table binary tree handling for MAsm.

Many of the source modules have associated header files. The important ones
which contain more than just function prototypes are:

cpu.h       Main simulator header file. Contains the main DLX structure
            definition, the pipeline unit structures, timer structure, defines
            for all CPU type and valid microcode field type constants.

machine.h   Each machine style has its own version of this file. It contains a
            single define so that platform dependent code is only compiled on
            the relevant platform.

micro.h     The sizes of the four microcode table components are defined here.
            It is used by both MAsm and the microcode simulator.

system.h    Includes all of the standard C #include files. Defines some
            standard variable types such as LONG and UBYTE.

The executables are made up as follows:
dasm        dasm.c dasm2.c dsym.c dasmlink.c

masm        masm.c msym.c masmlink.c

mon         cpu.c cpuh.c cpum.c cpup.c dis.c help.c ini.c io.c mem.c mon.c
            mon2.c mon3.c monlink.c

A separate utility called udlink was used to process the *link.c files. Every
time a program was linked it also contained the date of compilation plus a
constantly incrementing link number. This was only used on the Amiga version
which is where 99% of the development of DAsm, MAsm and Mon originally took
place. This enabled the Amiga, UNIX and PC version numbers to remain in
synchronisation.


State of the Code
~~~~~~~~~~~~~~~~~

The hardwired and microcode simulators are considered to be complete. However,
there is more work to do on the pipelined simulator with regard to interrupt
handling. Currently, interrupts are detected but do not execute or return
properly.

The assemblers are both complete although some extra features such as local
labels could be added to DAsm so that macros could contain labels. However,
this lack does not prevent DAsm from accomplishing its main task.

Earlier this year I attempted to update some of the code and examples. It was
only partially done so you may find two styles of commenting around!

Not included in this release is some code developed on the Mac which used a
graphical front end. It was not very successful and I no longer have (or want!)
access to a Mac. If anyone wants this code I will supply it upon request.

Compiling and Using
~~~~~~~~~~~~~~~~~~~

To compile the executables then unpack the tarball and do the following:

    make dasm
    make masm
    make mon

Then compile the DLX source code:

    ./dasm -fafiles

This will send the file list contained within the file 'afiles' to dasm which
will create all of the DLX object files that can be loaded into mon.

Now create the microcode table:

    ./masm hp.m

for the default microcode table (the H&P one) or use

    ./masm new.m

for an enhanced version. Note that the output file will always be called
dlxmcode.tbl (unless the -o switch is used). Both dasm and masm will show their
required inputs if no parameters are sent to them.


