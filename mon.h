/*

        DLX RISC Simulator - Monitor Header

        D. J. Viner

        Updates
1.3.0   05.09.1995    	Created
1.3.1   14.09.1995    	Added DebugLevel and defines
1.4.0   29.09.1995    	Upped version number for revision 4 opcodes
                    	PauseForKey prototype added
1.5.0   12.10.1995    	Upped version number for revision 5 opcodes
1.5.1   13.10.1995    	Removed DB_MARMDR (no longer needed)
1.5.2   19.10.1995    	Removed main version number (now only in system.h)
1.5.3   24.10.1995    	Added BPPC extern
1.5.4   26.10.1995    	Moved breakpoint stuff into cpu.h
1.5.5   01.12.1995    	PauseForKey prototype changed
1.5.6   04.12.1995    	Added DB_PIPESTAGES & DB_PIPEREGS
1.5.7   23.01.1996    	Added Log
1.5.8   28.01.1996    	Mods to PauseForKey prototype
1.5.9   30.04.1996    	Added DB_EXONLY

*/

extern  ULONG   LastAddr;

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* mon2.c                                                                 */
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

extern  VOID    DisplayDPRegisters ();
extern  VOID    DisplayIRegisters ();
extern  VOID    DisplayRegisters ();
extern  VOID    DisplaySettings ();
extern  VOID    DisplaySPRegisters ();
extern  VOID    DisplayStatRegisters ();
extern  VOID    DisplayTitle ();
extern  VOID    DoHelp ();
extern  ULONG   ExtractDecNo (STRPTR Str, WORD *Pos, BOOL *Blank);
extern  ULONG   ExtractNo (STRPTR Str, WORD *Pos, BOOL *Blank);
extern  UBYTE   PauseForKey (UBYTE Msg);

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* mon3.c                                                                 */
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

extern  ULONG   DebugLevel;
extern  FILE    *Log;

#define DB_DISASSEM     0x0001
#define DB_MICROCODE    0x0002
#define DB_REGS         0x0004
#define DB_PIPESTAGES   0x0008
#define DB_PIPEREGS     0x0010
#define DB_EXONLY       0x0020

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


