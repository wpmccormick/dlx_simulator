/////////////////////////////////////////////////////////////////////////////////////////
//
// DLX Assembler - Main Header file
//
// D. J. Viner
//
/////////////////////////////////////////////////////////////////////////////////////////
//
//  1.0.0   26.06.1995    	Created from 6502 asm.h
//  1.2.0   24.07.1995    	Mods for Unix gcc environment
//  1.2.1   27.07.1995    	Now calls system.h
//  1.5.0   15.12.1995    	Renamed from asm.h to dasm.h
//	1.5.1	31.03.2001		Improved code layout and removed 'UEA' stuff.
//
/////////////////////////////////////////////////////////////////////////////////////////

#include "system.h"

#define LINELEN     130

struct FileDetails
{
    UBYTE   FName [50];
    FILE    *In;
    ULONG   LineNo;
    struct  FileDetails *Prev;
};

extern FILE     *In;                    // Current input file
extern FILE     *Op;                    // Output file
extern FILE     *Lst;                   // Listing file

extern struct   FileDetails *Fd;

extern UBYTE    Pass, Line [LINELEN + 2];
extern UBYTE    FName [50], OName [50];
extern ULONG    Addr, LineNo, Addr2;
extern BOOL     Debug, PutAddr, Listing, MainLoop, ErrorSym, CodeGen,
                DoList, LittleEndian;
extern STRPTR   ptr;

/////////////////////////////////////////////////////////////////////////////////////////
