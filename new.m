;-----------------------------------------------------------------------;
;                                                                       ;
; Enhanced microcode (based on H&P code)                                ;
;                                                                       ;
; DJV   27.07.95    Created standard H&P code                           ;
; DJV   02.09.95    Added new MOV code                                  ;
; DJV   12.09.95    Added RFE, vectored TRAP and interrupt code         ;
; DJV   28.09.95    Added LD and LF coding                              ;
; DJV   13.10.95    Changed to reflect removal of RegXFs                ;
; DJV   23.01.96    Mods for interrupt problems in Intrpt & RFE         ;
;                                                                       ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; Lab    Dest  ALU     S1    S2    Const  Misc      Cond        Jump    ;
;-----------------------------------------------------------------------;

;-----------------------------------------------------------------------;
; Fig 5.23 Page 230                                                     ;
;-----------------------------------------------------------------------;

Ifetch,  ,     ,       ,     ,       ,    ,         Interrupt,  Intrpt
Iloop,   ,     ,       ,     ,       ,    InstrRd,  Mem,        Iloop
,        PC,   ADD,    PC,   Const,  4,   AB<-RF,   Decode1,
Intrpt,  IAR,  PassS1, PC,   ,       ,    ,         ,
,        PC,   PassS2, ,     Const,  0,   ,         Uncond,     Ifetch

;-----------------------------------------------------------------------;
; Fig 5.25 Page 231                                                     ;
;-----------------------------------------------------------------------;

Mem,     MAR,  ADD,    A,    imm16,  ,    ,         Load,       Load
Store,   MDR,  PassS2, ,     B,      ,    ,         ,
Dloop,   ,     ,       ,     ,       ,    DataWr,   Mem,        Dloop
,        ,     ,       ,     ,       ,    ,         Uncond,     Ifetch
Load,    ,     ,       ,     ,       ,    DataRd,   Mem,        Load
,        ,     ,       ,     ,       ,    ,         Decode2,
LB,      Temp, SLL,    MDR,  Const,  24,  ,         ,
,        C,    SRA,    Temp, Const,  24,  ,         Uncond,     Write1
LBU,     Temp, SLL,    MDR,  Const,  24,  ,         ,
,        C,    SRL,    Temp, Const,  24,  ,         Uncond,     Write1
LH,      Temp, SLL,    MDR,  Const,  16,  ,         ,
,        C,    SRA,    Temp, Const,  16,  ,         Uncond,     Write1
LHU,     Temp, SLL,    MDR,  Const,  16,  ,         ,
,        C,    SRL,    Temp, Const,  16,  ,         Uncond,     Write1
LW,      C,    PassS1, MDR,  ,       ,    ,         Uncond,     Write1
LF,      C,    PassS1, MDR,  ,       ,    ,         Uncond,     Write1
LD,      CD,   PassS1, MDR,  ,       ,    ,         Uncond,     Write1

;-----------------------------------------------------------------------;
; New MOV Code from Question.Doc                                        ;
;-----------------------------------------------------------------------;

MovI2S,  ,     ,       ,     ,       ,    ,         DestIAR,    DoIAR1
,        ,     ,       ,     ,       ,    ,         DestSR,     DoSR1
,        FPSR, PassS1, A,    ,       ,    ,         Uncond,     Ifetch
DoIAR1,  IAR,  PassS1, A,    ,       ,    ,         Uncond,     Ifetch
DoSR1,   SR,   PassS1, A,    ,       ,    ,         Uncond,     Ifetch
MovS2I,  ,     ,       ,     ,       ,    ,         SrcIAR,     DoIAR2
,        ,     ,       ,     ,       ,    ,         SrcSR,      DoSR2
,        C,    PassS1, FPSR, ,       ,    ,         Uncond,     Write1
DoIAR2,  C,    PassS1, IAR,  ,       ,    ,         Uncond,     Write1
DoSR2,   C,    PassS1, SR,   ,       ,    ,         ,

Write1,  ,     ,       ,     ,       ,    Rd<-C,    Uncond,     Ifetch

;-----------------------------------------------------------------------;
; Fig 5.27 Page 233                                                     ;
;-----------------------------------------------------------------------;

Reg,     Temp, PassS2, ,     B,      ,    ,         Decode2,
Imm,     Temp, PassS2, ,     imm16,  ,    ,         Decode3,
ADD/I,   C,    ADD,    A,    Temp,   ,    ,         Uncond,     Write2
SUB/I,   C,    SUB,    A,    Temp,   ,    ,         Uncond,     Write2
AND/I,   C,    AND,    A,    Temp,   ,    ,         Uncond,     Write2
OR/I,    C,    OR,     A,    Temp,   ,    ,         Uncond,     Write2
XOR/I,   C,    XOR,    A,    Temp,   ,    ,         Uncond,     Write2
SLL/I,   C,    SLL,    A,    Temp,   ,    ,         Uncond,     Write2
SRL/I,   C,    SRL,    A,    Temp,   ,    ,         Uncond,     Write2
SRA/I,   C,    SRA,    A,    Temp,   ,    ,         Uncond,     Write2
LHI,     C,    SLL,    Temp, Const,  16,  ,         Uncond,     Write2
Write2,  ,     ,       ,     ,       ,    Rd<-C,    Uncond,     Ifetch

;-----------------------------------------------------------------------;
; Fig 5.28 Page 233                                                     ;
;-----------------------------------------------------------------------;

SEQ/I,   ,     SUB,    A,    Temp,   ,    ,         Zero,       Set1
,        C,    PassS2, ,     Const,  0,   ,         Uncond,     Write4
SNE/I,   ,     SUB,    A,    Temp,   ,    ,         Zero,       Set0
,        C,    PassS2, ,     Const,  1,   ,         Uncond,     Write4
SLT/I,   ,     SUB,    A,    Temp,   ,    ,         Negative,   Set1
,        C,    PassS2, ,     Const,  0,   ,         Uncond,     Write4
SGE/I,   ,     SUB,    A,    Temp,   ,    ,         Negative,   Set0
,        C,    PassS2, ,     Const,  1,   ,         Uncond,     Write4
SGT/I,   ,     RSUB,   A,    Temp,   ,    ,         Negative,   Set1
,        C,    PassS2, ,     Const,  0,   ,         Uncond,     Write4
SLE/I,   ,     RSUB,   A,    Temp,   ,    ,         Negative,   Set1
,        C,    PassS2, ,     Const,  1,   ,         Uncond,     Write4
Set0,    C,    PassS2, ,     Const,  0,   ,         Uncond,     Write4
Set1,    C,    PassS2, ,     Const,  1,   ,         ,
Write4,  ,     ,       ,     ,       ,    Rd<-C,    Uncond,     Ifetch

;-----------------------------------------------------------------------;
; Fig 5.29 Page 234                                                     ;
;-----------------------------------------------------------------------;

Beq,     ,     SUB,    A,    Const,  0,   ,         Zero,       Branch
,        ,     ,       ,     ,       ,    ,         Uncond,     Ifetch
Bne,     ,     SUB,    A,    Const,  0,   ,         Zero,       Ifetch
Branch,  PC,   ADD,    PC,   imm16,  ,    ,         Uncond,     Ifetch
Jump,    PC,   ADD,    PC,   imm26,  ,    ,         Uncond,     Ifetch
JReg,    PC,   PassS1, A,    ,       ,    ,         Uncond,     Ifetch
JAL,     C,    PassS1, PC,   ,       ,    ,         ,
,        PC,   ADD,    PC,   imm26,  ,    R31<-C,   Uncond,     Ifetch
JALR,    C,    PassS1, PC,   ,       ,    ,         ,
,        PC,   PassS1, A,    ,       ,    R31<-C,   Uncond,     Ifetch

;-----------------------------------------------------------------------;
; TRAPS now VECTORED through low memory                                 ;
;-----------------------------------------------------------------------;

Trap,    IAR,  PassS1, PC,   ,       ,    ,         ,
,        MAR,  PassS2, ,     imm26,  ,    DataRd,   ,
,        PC,   PassS1, MDR,  ,       ,    ,         Uncond,     Ifetch

Rfe,     PC,   PassS1, IAR,  ,       ,    ,         ,
,        IAR,  PassS2, ,     Const,  0,   ,         Uncond,     Ifetch

#

;-----------------------------------------------------------------------;
; Decode1 table                                                         ;
;-----------------------------------------------------------------------;

Memory,     Mem
MOVI2S,     MovI2S
MOVS2I,     MovS2I
S2=B,       Reg
S2=Imm,     Imm
BEQZ,       Beq
BNEZ,       Bne
J,          Jump
JR,         JReg
JAL,        JAL
JALR,       JALR
TRAP,       Trap
RFE,        Rfe

#

;-----------------------------------------------------------------------;
; Decode2/3 table                                                       ;
;-----------------------------------------------------------------------;

LB,     LB
LBU,    LBU
LH,     LH
LHU,    LHU
LW,     LW
LF,     LF
LD,     LD
ADD,    ADD/I
SUB,    SUB/I
AND,    AND/I
OR,     OR/I
XOR,    XOR/I
SLL,    SLL/I
SRL,    SRL/I
SRA,    SRA/I
LHI,    LHI
SEQ,    SEQ/I
SNE,    SNE/I
SLT,    SLT/I
SGE,    SGE/I
SGT,    SGT/I
SLE,    SLE/I

;-----------------------------------------------------------------------;



