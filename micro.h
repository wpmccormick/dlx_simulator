/*

        DLX MicroAssembler/Cpu - Common header for microcode tables

        D. J. Viner

        Updates
1.2.0   01.08.1995    	Created
1.2.1   09.08.1995    	Upped MICROSIZE from 128 to 256
1.2.3   28.08.1995    	Simplified Decode1/2 tables
1.3.0   02.09.1995    	Moved tables into cpu.h and masm.c
                    	Moved Decode defines into cpum.h

*/

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
/* Define microcode and decode table sizes                                */
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#define MICROSIZE   256
#define DECODE1SIZE  48
#define DECODE2SIZE  48

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/


