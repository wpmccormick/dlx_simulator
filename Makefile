###### Compiler, tools and options

CC	=	gcc
CXX	=	g++
LINK	=	g++
LIBS	=	-lm
CFLAGS  =

####### Files

MONOBJS= cpu.o cpuh.o cpum.o cpup.o mem.o mon.o mon2.o mon3.o dis.o ini.o help.o io.o monlink.o

DASMOBJS= dsym.o dasm.o dasm2.o dasmlink.o

MASMOBJS= msym.o masm.o masmlink.o

###### Implicit rules

.SUFFIXES: .c

.c.o:
	$(CC) -c $(CFLAGS) $(INCPATH) -o $@ $<

####### Build rules


all: mon masm dasm

clean:
	-rm -f $(MONOBJS) $(DASMOBJS) $(MASMOBJS) mon dasm masm
	-rm -f *~ core

####### Compile

mon: $(MONOBJS)
	$(LINK) $(LFLAGS) -o mon $(MONOBJS)  $(LIBS)

dasm: $(DASMOBJS)
	$(LINK) $(LFLAGS) -o dasm $(DASMOBJS)  $(LIBS)

masm: $(MASMOBJS)
	$(LINK) $(LFLAGS) -o masm $(MASMOBJS)  $(LIBS)

cpu.o: cpu.c cpu.h system.h

cpuh.o: cpuh.c cpu.h system.h

cpum.o: cpum.c cpu.h micro.h system.h

cpup.o: cpup.c cpu.h system.h

dasm.o: dasm.c dasm.h dasm2.h dsym.h system.h

dasm2.o: dasm2.c dasm.h dasm2.h dsym.h system.h

dis.o: dis.c cpu.h system.h dis.h

dsym.o: dsym.c dasm.h dsym.h system.h

help.o: help.c cpu.h system.h

ini.o: ini.c cpu.h system.h

io.o: io.c cpu.h system.h

masm.o: masm.c masm.h msym.h micro.h system.h

mem.o: mem.c cpu.h system.h

mon.o: mon.c cpu.h dis.h mon.h system.h

mon2.o: mon2.c cpu.h mon.h system.h

mon3.o: mon3.c cpu.h dis.h mon.h system.h micro.h

msym.o: msym.c masm.h msym.h system.h

dasmlink.o: dasmlink.c

masmlink.o: masmlink.c

monlink.o: monlink.c




