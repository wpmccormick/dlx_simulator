/*

        DLX RISC Simulator - Monitor Disassembler header

        D. J. Viner

        Updates
1.1.0   11.07.1995    	Created

*/

extern  VOID    Disassemble (STRPTR Cmd);
extern  VOID    DisassembleInstr (ULONG Addr, ULONG Instr);


