/*

        DLX RISC Simulator - File input/output header

        D. J. Viner

        Updates
1.5.0   15.12.1995    	Created

*/

extern  BOOL    GetFileName (STRPTR Cmd, WORD *Pos, STRPTR FileName);
extern  VOID    DoLoad (STRPTR Cmd, BOOL Display);
extern  VOID    DoSave (STRPTR Cmd);



