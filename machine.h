/*

        Machine type header - contains define for system running on
        which can be AMIGA, Pc, UNIX or MAC - the Amiga does not
        require a define

        D. J. Viner

        Updates
1.5.0   24.01.1996    	Created

*/

#undef	AMIGA
#undef	MAC
#undef	Pc
#define UNIX
